#include <Servo.h>
#include "NewPing.h"
#include "UltraCar_IO.h"
#include "UltraCar_Direction.h"
Servo myServo;

int lookRight() {
  myServo.write(10);
  delay(500);
  int distance = readPing();
  delay(100);
  myServo.write(90);
  return distance;
}

int lookLeft() {
  myServo.write(170);
  delay(500);
  int distance = readPing();
  delay(100);
  myServo.write(90);
  return distance;
  delay(100);
}

int readPing() {
  delay(70);
  int cm = sonar.ping_cm();
  if (cm == 0) {
    cm = 250;
  }
  return cm;
}

void setup() {
  myServo.attach(_SERVO);
  myServo.write(90);
  delay(500);
  distance = readPing();
  delay(100);
  distance = readPing();
  delay(100);
  distance = readPing();
  delay(100);
  distance = readPing();
  delay(100);
  pinMode(IN_1, OUTPUT);
  pinMode(IN_2, OUTPUT);
  pinMode(IN_3, OUTPUT);
  pinMode(IN_4, OUTPUT);
  delay(10);
}


void loop() {
  if (!startA) {
    startA = true;
    randomStart = random(1, 2);
    callRandomStart(randomStart);
    delay(50);
  }
  int distanceRight = 0;
  int distanceLeft = 0;
  delay(50);

  if (distance <= 40) {
    stopRobot();
    delay(100);

    goBack();
    delay(random(200, 300));
//    delay(100);
    
    
    
    stopRobot();
    delay(50);
    distanceRight = lookRight();
    delay(300);
    distanceLeft = lookLeft();
    delay(300);

    if (distance >= distanceLeft) {
      goRandomRight = random(1, 2);
      callRandomRight(goRandomRight);
      stopRobot();
    }
    else {
      goRandomLeft = random(1, 2);
      callRandomLeft(goRandomLeft);
      stopRobot();
    }
  }
  else {
    goAhead();
  }
  distance = readPing();
}

void callRandomLeft(int goRandomLeft) {
  if (goRandomLeft == 1) {
    goBackLeft();
    delay(random(300, 600));
    goCircleLeft();
    delay(random(300, 600));
  }
  if (goRandomLeft == 2) {
    goCircleLeft();
    delay(random(300, 600));
    goBackLeft();
    delay(random(300, 600));
  }
}

void callRandomRight(int goRandomRight) {
  if (goRandomRight == 1) {
    goBackRight();
    delay(random(300, 600));
    goCircleRight();
    delay(random(300, 600));
  }
  if (goRandomRight == 2) {
    goCircleRight();
    delay(random(300, 600));
    goBackRight();
    delay(random(300, 600));
  }
}

void callRandomStart(int randomStart) {
  if (randomStart == 1) {
    goCircleRight();
    delay(500);
    goCircleLeft();
    delay(500);
    goAhead();
  }
  if (randomStart == 2) {
    goBackLeft();
    delay(500);
    goBackRight();
    delay(500);
    goAhead();
  }
}
