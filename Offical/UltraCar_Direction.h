
int speedCar = 190;
int speedCar_half = 2;

void goAhead() {
  if (!goesAhead) {
    goesAhead = true;
    analogWrite(IN_1, speedCar);
    analogWrite(IN_2, 0);
    analogWrite(IN_3, speedCar);
    analogWrite(IN_4, 0);
  }
}
void goBack() {
  goesAhead = false;
  analogWrite(IN_1, 0);
  analogWrite(IN_2, speedCar);
  analogWrite(IN_3, 0);
  analogWrite(IN_4, speedCar);
}

void goCircleRight() {
  analogWrite(IN_1, speedCar);
  analogWrite(IN_2, 0);
  analogWrite(IN_3, 0);
  analogWrite(IN_4, speedCar);
}
void goCircleLeft() {
  analogWrite(IN_1, 0);
  analogWrite(IN_2, speedCar);
  analogWrite(IN_3, speedCar);
  analogWrite(IN_4, 0);
}
void goBackRight() {

  analogWrite(IN_1, 0);
  analogWrite(IN_2, speedCar);
  analogWrite(IN_3, 0);
  analogWrite(IN_4, speedCar / speedCar_half);
}


void goBackLeft() {
  analogWrite(IN_1, 0);
  analogWrite(IN_2, speedCar / speedCar_half);
  analogWrite(IN_3, 0);
  analogWrite(IN_4, speedCar);
}


void stopRobot() {
  analogWrite(IN_1, 0);
  analogWrite(IN_2, 0);
  analogWrite(IN_3, 0);
  analogWrite(IN_4, 0);
}
